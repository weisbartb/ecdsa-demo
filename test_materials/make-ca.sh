#!/usr/bin/env bash
# If you run this on windows make sure you do ``MSYS_NO_PATHCONV=1 ./make-ca.sh`` or weird things will happen
declare -a services=("authentication" "user" "communication" "demo")

echo "Please provide the name of this CA [Enter to finish]"
read caname
echo "Please provide an CN for the CA [Enter to finish]"
read commonname
echo "Please provide an OU for the CA [Enter to finish]"
read orgunit
echo "Please provide an org name for the CA [Enter to finish]"
read orgname


if [[ ${#caname} -lt 1 ]]; then
  echo "Invalid CA name provided"; 
  exit 
fi

if [[ ! -d ${caname} ]]; then
  mkdir ${caname}
fi
cd ${caname}
if [[ ! -f ${caname}-ca.key ]]; then
# Go doesn't like ECparams being present, neither do several other things, noout will remove this
  openssl ecparam -genkey -name prime256v1 -param_enc named_curve -out ${caname}-ca.key -noout
  openssl req -x509 -new -nodes -key ${caname}-ca.key -subj "/C=On/ST=The/L=Internet/O=${orgname}/OU=${orgunit}/CN=${commonname}" -sha256 -days 1024 -out ${caname}-ca.crt
fi 
arraylength=${#services[@]}
if [[ ! -d services ]]; then
  mkdir services
  mkdir services/keys
  mkdir services/certs
  mkdir services/csrs
fi
for (( i=1; i<${arraylength}+1; i++ ));
do
  serviceName=${services[$i-1]}
  echo ${serviceName}
  # Go doesn't like ECparams being present, neither do several other things, noout will remove this
  if [[ ! -f services/keys/${serviceName}.key ]]; then
    openssl ecparam -genkey -name prime256v1 -param_enc named_curve -out services/keys/${serviceName}.key -noout
    chmod 0600 services/keys/${serviceName}.key
    openssl req -new -sha256 -key services/keys/${serviceName}.key -subj "/C=On/ST=The/L=Internet/O=${orgname}/OU=${orgunit}/CN=${serviceName}" -out services/csrs/${serviceName}.csr
    openssl x509 -req -in services/csrs/${serviceName}.csr -CA ${caname}-ca.crt -CAkey ${caname}-ca.key -CAcreateserial -out "services/certs/${serviceName}.crt" -days 500 -sha256
  fi
done
