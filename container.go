package ecdsaDemo

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"sync/atomic"
	"time"
	"unsafe"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
)

// The service header key that identifies the authentication service
// This is what we would look for in the header of a token to find out who signed it
// The certificate would also have this set as the common name, if it isn't set to this it can't find it in the
// 	container that holds all of the certificates
const AuthServiceKey = "authentication"

// The header key inside the token that identifies the service so that its public key can be loaded and compared
const serviceHeaderKey = "service"

// Used for swap contention limiters
const contentionLimit = 50

// A container to store all the public keys in so that we can CAS
type swappableKeyContainer struct {
	keys map[string]*ecdsa.PublicKey
}

// Get a key from the container
func (c swappableKeyContainer) Get(key string) (val *ecdsa.PublicKey, found bool) {
	if c.keys == nil {
		return nil, false
	}
	val, found = c.keys[key]
	return
}

// Authentication container
// Notes: Services should invoke create container rather than making this directly.
type AuthContainer struct {
	// The private key that is used by the module
	privateKey *ecdsa.PrivateKey
	// The CA x509 certificate
	caCert *x509.Certificate
	// The directory to load keys from
	loadKeysFrom string
	// The service name to
	serviceName string
	// The logger service
	logger zerolog.Logger
	// interior container, the reason this exists this way so that a CAS op can flip the container on reload
	knownKeys *swappableKeyContainer
	// List of old keys
	oldKeys *swappableKeyContainer
	// Are old keys currently used - the a uint is to provide a counter in the event multiple swaps happen at the same time
	oldKeysInUse int32
	// a lock to help synchronize buffer purges
	bufferPurgeMu *sync.Mutex
}

// Create a new authentication container
// Arguments:
// 	privateKeyPath - the path to your private key for your service (used to create service tokens)
// 	caCert - the CA certificate to verify all certificates against to ensure that no rogue certs appear
// 	loadKeysFrom - the directory to load all of the public key/certificates from
//  logger - an instance of the logger to use
//  serviceName - the name of the service
//
// Returns:
// 	An authentication container that can be used to verify/authenticate/create requests
//  Any error that occurred during the creation of the container
func CreateContainer(privateKeyPath, caCert string, loadKeysFrom string, logger zerolog.Logger, serviceName string) (*AuthContainer, error) {
	if len(serviceName) == 0 {
		return nil, fmt.Errorf("a service name is required to create a container")
	}
	if info, err := os.Stat(loadKeysFrom); err != nil || !info.IsDir() {
		return nil, fmt.Errorf("invalid cert storage path provided (%v)", loadKeysFrom)
	}
	data, err := ioutil.ReadFile(privateKeyPath)
	if err != nil {
		return nil, err
	}
	decodedPemData, _ := pem.Decode(data)
	key, err := x509.ParseECPrivateKey(decodedPemData.Bytes)
	if err != nil {
		return nil, err
	}
	container := &AuthContainer{
		privateKey:    key,
		logger:        logger.With().Str("module", "authentication").Logger(),
		knownKeys:     &swappableKeyContainer{make(map[string]*ecdsa.PublicKey)},
		serviceName:   serviceName,
		bufferPurgeMu: &sync.Mutex{},
	}
	data, err = ioutil.ReadFile(caCert)
	if err != nil {
		return nil, err
	}
	caPemData, _ := pem.Decode(data)
	cert, err := x509.ParseCertificate(caPemData.Bytes)
	if err != nil {
		return nil, err
	}
	container.caCert = cert
	container.loadKeysFrom = loadKeysFrom
	container.RefreshKeys(time.Duration(1))
	return container, nil
}

// Change the certificate directory
// Arguments:
// 	loadKeysFrom - what directory to load keys from on the next reload
//
// Returns:
// 	any errors from changing that certificate directory
func (container *AuthContainer) ChangeCertDir(loadKeysFrom string) error {
	if info, err := os.Stat(loadKeysFrom); err != nil || !info.IsDir() {
		return fmt.Errorf("invalid cert storage path provided (%v)", loadKeysFrom)
	}
	container.loadKeysFrom = loadKeysFrom
	return nil
}

// Verify that a service token is valid and decodes it to a usable token struct
// Arguments:
// 	rawToken - the byte array that represent the token
//
// Returns:
// 	A service bearer token
//  Any errors that occurred during the decoding of the token
func (container *AuthContainer) VerifyServiceToken(rawToken []byte) (*ServiceToken, error) {
	// Tokens should be formatted correctly - but sometimes they are not
	if bytes.Equal(bytes.ToLower(rawToken[0:7]), []byte("bearer ")) {
		rawToken = rawToken[7:]
	}
	claims := &ServiceToken{}
	token, err := jwt.ParseWithClaims(string(rawToken), claims, func(token *jwt.Token) (key interface{}, err error) {
		defer func() {
			if tErr := recover(); tErr != nil {
				err, _ = tErr.(error)
			}
		}()
		var ok bool
		if _, ok = token.Method.(*jwt.SigningMethodECDSA); !ok {
			err = fmt.Errorf("only ECDSA signed tokens are allowed")
			return
		}
		if signingServiceInterface, ok := token.Header[serviceHeaderKey]; ok {
			if signingService, ok := signingServiceInterface.(string); ok {
				if key, ok = container.knownKeys.Get(signingService); ok && key != nil {
					return
				} else if container.oldKeysInUse > 0 {
					if key, ok = container.oldKeys.Get(signingService); ok && key != nil {
						return
					}
				}
				if key == nil {
					err = fmt.Errorf("illegal key provided for service %v", token.Header[serviceHeaderKey].(string))
					return
				}
			}
		}
		err = fmt.Errorf("could not find service - %v in cert bundle", token.Header[serviceHeaderKey].(string))
		return
	})
	if err != nil {
		return nil, err
	}
	claims.Issuer = token.Header[serviceHeaderKey].(string)
	return claims, nil
}

// Generate a new service token to be used
// Arguments:
// 	timeout - how long is the token valid for
//
// Returns:
// 	An encoded token string
//  Any error that occurred during the generation

func (container *AuthContainer) GenerateServiceToken(timeout time.Duration) (encodedToken string, err error) {
	now := time.Now().UTC()
	id := uuid.New().String()
	token := jwt.NewWithClaims(jwt.SigningMethodES256, ServiceToken{
		TokenId: id,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  now.Unix(),
			ExpiresAt: now.Add(timeout).Unix(),
			Issuer:    container.serviceName,
			Id:        id,
			NotBefore: now.Unix(),
		},
	})
	token.Header[serviceHeaderKey] = container.serviceName
	encodedToken, err = token.SignedString(container.privateKey)
	return
}

// Check to see if a service certificate exists in the platform
// Arguments:
// 	key - the name of the service you are trying to see exists
//
// Returns:
// 	Does the service key exist?
func (container *AuthContainer) ServiceKeyExists(key string) bool {
	if container.knownKeys == nil {
		return false
	}
	_, ok := container.knownKeys.keys[key]
	return ok
}

// Get the number of keys loaded in the platform
// Returns:
// 	The number of keys in the container
func (container *AuthContainer) NumberOfKeysLoaded() int {
	return len(container.knownKeys.keys)
}

// Get a list of all service keys that are loaded
// Returns:
//	A list of service keys
func (container *AuthContainer) ListOfServiceKeys() []string {
	resp := make([]string, 0)
	for serviceId := range container.knownKeys.keys {
		resp = append(resp, serviceId)
	}
	return resp
}
func (container *AuthContainer) FlushKeys(keepKeysFor time.Duration) {
	newContainer := swappableKeyContainer{
		keys: map[string]*ecdsa.PublicKey{},
	}
	container.swapKeys(&newContainer, keepKeysFor)
}

// Refresh all of the keys from the target certificate directory
// Arguments:
// 	keepKeysFor - the duration to hold onto the keys before dumping them, both the new keys and old keys will be valid
func (container *AuthContainer) RefreshKeys(keepKeysFor time.Duration) {
	newContainer := swappableKeyContainer{
		keys: map[string]*ecdsa.PublicKey{},
	}
	_ = filepath.Walk(container.loadKeysFrom, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() && strings.Contains(info.Name(), ".crt") {
			tKey, err := container.loadKey(path)
			var ok bool
			if tKey == nil {
				container.logger.Error().Msgf("Invalid key type (need ecc) - %v - %v", info.Name(), err)
				return nil
			}
			if err == nil {
				var tmpKey *ecdsa.PublicKey
				if tmpKey, ok = (tKey.PublicKey).(*ecdsa.PublicKey); !ok {
					container.logger.Error().Msgf("Invalid key type (need ecc) - %v - %v", info.Name(), err)
					return nil
				} else {
					newContainer.keys[tKey.Subject.CommonName] = tmpKey
				}
			}
		}
		return nil
	})
	container.logger.Info().Msgf("Loading %v keys into the ring", len(newContainer.keys))
	// Todo - add more configurable timeouts
	container.swapKeys(&newContainer, time.Second*30)
}

func (container *AuthContainer) PurgeOldKeys(force bool) {
	container.bufferPurgeMu.Lock()
	defer container.bufferPurgeMu.Unlock()
	// It is possible for this to cause issues with heavy reload contention
	pendingKeySwaps := atomic.AddInt32(&container.oldKeysInUse, -1)
	var ok bool
	if pendingKeySwaps <= 0 || force {
		// Drain down that container
	expiringKeyCAS:
		for x := 0; x < contentionLimit; x++ {
			expiringContainer := container.oldKeys
			// This can't be nil as oldKeys might get called after the CAS goes off due to a potential race condition, so we just make a safe object with no data inside it
			emptyContainer := swappableKeyContainer{
				keys: map[string]*ecdsa.PublicKey{},
			}
			if atomic.CompareAndSwapPointer((*unsafe.Pointer)(unsafe.Pointer(&container.oldKeys)), unsafe.Pointer(expiringContainer), unsafe.Pointer(&emptyContainer)) {
				go func() {
					time.Sleep(time.Second * 3)
					for k := range expiringContainer.keys {
						delete(expiringContainer.keys, k)
					}
					expiringContainer = nil
				}()
				ok = true
				break expiringKeyCAS
			}

		}
		if ok == false {
			container.logger.Error().Msgf("CAS operation for purging the expired container has failed")
		}
	}
}
func (container *AuthContainer) swapKeys(newContainer *swappableKeyContainer, keepKeysFor time.Duration) {

	// New keys swapped in?
	newOk := false
newKeyCAS:
	for i := 0; i < contentionLimit; i++ {
		oldContainer := container.knownKeys
		// Swap the first container
		if atomic.CompareAndSwapPointer((*unsafe.Pointer)(unsafe.Pointer(&container.knownKeys)), unsafe.Pointer(oldContainer), unsafe.Pointer(newContainer)) {
			// Swap in the old container

			atomic.SwapPointer((*unsafe.Pointer)(unsafe.Pointer(&container.oldKeys)), unsafe.Pointer(oldContainer))
			if keepKeysFor > 0 {
				atomic.AddInt32(&container.oldKeysInUse, 1)
				go func() {
					// After this time we will wake up and see if we can purge the old keys
					time.Sleep(keepKeysFor)
					// Need to lock a mutex to prevent a race condition since go doesn't have a universal STW call that can be made
					container.PurgeOldKeys(false)
				}()

			} else {
				container.PurgeOldKeys(true)
			}
			newOk = true
			break newKeyCAS
		}

	}
	if !newOk {
		container.logger.Error().Msgf("CAS operation on newKeys failed")
	}
	if newOk {
		container.logger.Info().Msg("Keys reloaded")
	}
}

// Load a key from a specific source
func (container *AuthContainer) loadKey(certLocation string) (tKey *x509.Certificate, err error) {
	var certData []byte
	certData, err = ioutil.ReadFile(certLocation)
	if err != nil {
		container.logger.Error().Msgf("Can not load file - %v - %v", certLocation, err)
		return
	}
	block, _ := pem.Decode(certData)
	tKey, err = x509.ParseCertificate(block.Bytes)
	if err != nil {
		container.logger.Error().Msgf("Can not parse certificate - %v - %v", certLocation, err)
		return
	}
	err = tKey.CheckSignatureFrom(container.caCert)
	if err != nil {
		container.logger.Error().Msgf("Signature mismatch - %v - %v", certLocation, err)
		return
	}
	return
}
