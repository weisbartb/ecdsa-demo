module bitbucket.org/weisbartb/ecdsa-demo

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/uuid v1.0.0
	github.com/rs/zerolog v1.10.1
)
