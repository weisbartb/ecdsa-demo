package ecdsaDemo

import (
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"path/filepath"
	"runtime"
	"testing"
)

func TestLoadKey(t *testing.T) {
	_, file, _, _ := runtime.Caller(0)
	basePath := filepath.Dir(file)
	data, err := ioutil.ReadFile(basePath + string(filepath.Separator) + "/test_materials/demo/demo-ca.crt")
	if err != nil {
		t.Errorf("Could not open the demo-ca.crt file - %v", err)
		t.FailNow()
	}
	block, _ := pem.Decode(data)
	caCert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		t.Errorf("Could not parse the demo-ca.crt file - %v", err)
		t.FailNow()
	}
	data, err = ioutil.ReadFile(basePath + string(filepath.Separator) + "/test_materials/demo/services/certs/demo.crt")
	if err != nil {
		t.Errorf("Could not open the demo.crt file - %v", err)
		t.FailNow()
	}
	block, _ = pem.Decode(data)
	testCert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		t.Errorf("Could not parse the test.crt file - %v", err)
		t.FailNow()
	}
	err = testCert.CheckSignatureFrom(caCert)
	if err != nil {
		t.Errorf("Could not verify signature from known file - %v", err)
	}
}
func TestLoadUnsignedKey(t *testing.T) {
	_, file, _, _ := runtime.Caller(0)
	basePath := filepath.Dir(file)
	data, err := ioutil.ReadFile(basePath + string(filepath.Separator) + "/test_materials/demo/demo-ca.crt")
	if err != nil {
		t.Errorf("Could not open the ca.crt file - %v", err)
		t.FailNow()
	}
	block, _ := pem.Decode(data)
	caCert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		t.Errorf("Could not parse the ca.crt file - %v", err)
		t.FailNow()
	}
	data, err = ioutil.ReadFile(basePath + string(filepath.Separator) + "/test_materials/demo2/services/certs/demo.crt")
	if err != nil {
		t.Errorf("Could not open the test.crt file - %v", err)
		t.FailNow()
	}
	block, _ = pem.Decode(data)
	testCert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		t.Errorf("Could not parse the test.crt file - %v", err)
		t.FailNow()
	}
	err = testCert.CheckSignatureFrom(caCert)
	if err == nil {
		t.Errorf("Certificate file should failed the signature check")
	}
}
