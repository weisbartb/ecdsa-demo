package ecdsaDemo

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

type ServiceToken struct {
	jwt.StandardClaims
	TokenId string `json:"tokenId"`
	Issuer  string `json:"issuer"`
}

func (token ServiceToken) GetSigningService() string {
	return token.Issuer
}
func (token ServiceToken) IsValidToken() bool {
	return token.TokenId != InvalidAuthenticationTokenId
}
func (token ServiceToken) Valid() error {
	now := time.Now().UTC().Unix()
	// We require expiration be set
	if token.ExpiresAt == 0 {
		return ErrExpectedExpiresAt
	}
	if now > token.ExpiresAt {
		return ErrTokenExpired
	}
	if token.IssuedAt == 0 {
		return ErrExpectedIssuedAt
	}
	if token.NotBefore > 0 && token.NotBefore < now {
		return ErrTokenNotValidYet
	}
	return nil
}
