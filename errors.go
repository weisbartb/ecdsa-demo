package ecdsaDemo

import "errors"

var ErrExpectedExpiresAt = errors.New("expected expiresAt to be set in the token, it was not present")
var ErrTokenExpired = errors.New("the provided token has expired")
var ErrExpectedIssuedAt = errors.New("expected to see issued at (iat) to be set in the token, it was not present")

var ErrTokenNotValidYet = errors.New("token is currently not valid yet")
