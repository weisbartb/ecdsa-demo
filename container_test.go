package ecdsaDemo_test

import (
	"path"
	"runtime"
	"testing"
	"time"

	"github.com/rs/zerolog"

	authentication "bitbucket.org/weisbartb/ecdsa-demo"
)

var whereAmI = func() string {
	_, file, _, _ := runtime.Caller(0)
	return path.Dir(file)
}()
var basePath = whereAmI + "/test_materials/demo"

func createValidContainer(logger zerolog.Logger) (*authentication.AuthContainer, error) {

	return authentication.CreateContainer(basePath+"/services/keys/authentication.key", whereAmI+"/test_materials/demo/demo-ca.crt", basePath+"/services/certs", logger, "authentication")
}

// Test creating various types of containers
func TestCreateContainer(t *testing.T) {
	// Create a container that should succeed
	logger := zerolog.Nop()
	_, err := createValidContainer(logger)
	if err != nil {
		t.Errorf("Could not create a new authentication container - %v", err)
		t.FailNow()
	}
	// Create a container with a directory that doesn't exist
	_, err = authentication.CreateContainer(whereAmI+"/test_materials/demo/services/keys/authentication.key", whereAmI+"/test_materials/demo/demo-ca.crt", whereAmI+"/test_materials/demo/services/asdasd", logger, "authentication")
	if err == nil {
		t.Errorf("Was able to create a container without a valid cert bundle directory")
		t.FailNow()
	}
	// Create a container with no service name
	_, err = authentication.CreateContainer(whereAmI+"/test_materials/demo/services/keys/authentication.key", whereAmI+"/test_materials/demo/demo-ca.crt", whereAmI+"/test_materials/demo/services/certs", logger, "")
	if err == nil {
		t.Errorf("Was able to create a container without a serivce name")
		t.FailNow()
	}
}

// Test refreshing keys in the container
func TestAuthContainer_RefreshKeys(t *testing.T) {

	// Create a container that should succeed
	logger := zerolog.Nop()
	container, err := authentication.CreateContainer(basePath+"/services/keys/authentication.key", whereAmI+"/test_materials/demo/demo-ca.crt", basePath+"/services/csrs", logger, "authentication")
	if err != nil {
		t.Errorf("Could not create a new authentication container - %v", err)
		t.FailNow()
	}
	time.Sleep(time.Microsecond * 100)
	if container.NumberOfKeysLoaded() != 0 {
		t.Errorf("Expected no keys to be loaded (invalid cert detected for baseline key)")
		t.FailNow()
	}
	if err = container.ChangeCertDir(whereAmI + "/test_materials/demo/services/certs"); err != nil {
		t.Error(err)
		t.FailNow()
	}
	container.RefreshKeys(time.Duration(1))
	time.Sleep(time.Second * 4)
	if container.NumberOfKeysLoaded() != 4 {
		t.Errorf("Expected to see all 4 keys that are in the services directory present in the key ring")
		t.FailNow()
	}
	if !container.ServiceKeyExists("authentication") {
		t.Errorf(`Expected to see service "auth" in the list of provided services.`)
		t.FailNow()
	}
}

// Test to ensure that the service token can be created and then verified
func TestAuthContainer_VerifyServiceToken(t *testing.T) {
	// Create a container that should succeed
	logger := zerolog.Nop()
	container, err := createValidContainer(logger)
	if err != nil {
		t.Errorf("Could not create a new authentication container - %v", err)
		t.FailNow()
	}
	token, err := container.GenerateServiceToken(time.Second * 30)
	if err != nil {
		t.Errorf("Could not create token - %v", err)
		t.FailNow()
	}
	_, err = container.VerifyServiceToken([]byte(token))
	if err != nil {
		t.Errorf("The token could not be verified - %v", err)
		t.FailNow()
	}
}

// Test to ensure that older key buffers are still valid
func TestAuthContainer_ReloadRace(t *testing.T) {
	logger := zerolog.Nop()
	container, err := createValidContainer(logger)
	if err != nil {
		t.Errorf("Could not create a new authentication container - %v", err)
		t.FailNow()
	}
	container.RefreshKeys(time.Duration(time.Millisecond * 50))
	container.RefreshKeys(time.Duration(time.Millisecond * 150))
	token, err := container.GenerateServiceToken(time.Second * 30)
	if err != nil {
		t.Errorf("Could not create token - %v", err)
		t.FailNow()
	}
	_, err = container.VerifyServiceToken([]byte(token))
	if err != nil {
		t.Errorf("The token could not be verified - %v", err)
		t.FailNow()
	}
	container.RefreshKeys(time.Duration(time.Millisecond * 500))
	container.RefreshKeys(time.Duration(time.Millisecond * 75))
	container.RefreshKeys(time.Duration(time.Second))
	time.Sleep(5 * time.Second)
	token, err = container.GenerateServiceToken(time.Second * 30)
	if err != nil {
		t.Errorf("Could not create token - %v", err)
		t.FailNow()
	}
	_, err = container.VerifyServiceToken([]byte(token))
	if err != nil {
		t.Errorf("The token could not be verified - %v", err)
		t.FailNow()
	}
	container.FlushKeys(0)
	token, err = container.GenerateServiceToken(time.Second * 30)
	if err != nil {
		t.Errorf("Could not create token - %v", err)
		t.FailNow()
	}
	_, err = container.VerifyServiceToken([]byte(token))
	if err == nil {
		t.Errorf("Was able to verify a dead service token")
		t.FailNow()
	}
}
